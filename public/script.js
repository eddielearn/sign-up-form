const submitButton = document.querySelector('.button');
const pass = document.querySelector('#password');
const confirmPass = document.querySelector('#confirm_password');

function passValidation() {
    if (pass.value != confirmPass.value) {
        confirmPass.style.border = '2px solid red';
    } else {
        confirmPass.style.border = '2px solid green';
    }
}

submitButton.addEventListener('click', e => {
    e.preventDefault();
    passValidation();
  });